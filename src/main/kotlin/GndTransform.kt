/*
 * gnd-import
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.KlaxonException
import org.apache.kafka.streams.KeyValue
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.io.FileInputStream
import java.io.StringReader
import java.net.URLDecoder
import java.nio.charset.Charset
import java.util.*
import java.util.regex.Pattern

class GndTransform(properties: Properties, private val log: Logger) {
    private val mapping: JsonObject = Klaxon().parseJsonObject(
        FileInputStream(properties.getProperty("kafka.index.mapping.path")).reader(
            Charset.defaultCharset()))

    private val yearPattern = Pattern.compile("(-?\\d{4})(-\\d{2})?(-\\d{2})?")

    fun parseJson(model: SbMetadataModel): List<DataStructure> {
        return try {
            val result = Klaxon().parseJsonObject(StringReader(model.data))
            listOf(DataStructure(model.messageDate, result))
        } catch (ex: KlaxonException) {
            log.error("Dropped message due to parse error ${ex.message}. With data: ${model.data}")
            emptyList()
        }
    }

    fun extractKey(structure: DataStructure): List<KeyValue<String, DataStructure>> {
        val inputKey = structure.data.string("id")
        return if (inputKey != null) {
            listOf(KeyValue(inputKey, structure))
        } else {
            log.error("Dropped message without id. ${structure.data}")
            emptyList()
        }
    }


    fun extractTypes(key: String, structure: DataStructure): List<DataStructure2> {
        return if (structure.data.containsKey("type")) {
            val types = structure.data.array<String>("type")
            return when {
                types != null -> listOf(DataStructure2(structure.date, JsonArray(types), structure.data))
                else -> {
                    log.error("Dropped message without types with key $key.")
                    emptyList()
                }
            }
        } else {
            log.error("Dropped message without types with key $key.")
            emptyList()
        }
    }

    fun expandTypes(structure: DataStructure2): DataStructure2 {
        val expandedTypes = JsonArray(emptyList<String>())
        for (type in structure.types) {
            expandedTypes.add("https://d-nb.info/standards/elementset/gnd#$type")
        }
        structure.data["type"] = expandedTypes
        return DataStructure2(structure.date, structure.types, structure.data)
    }

    fun replaceGender(structure: DataStructure2): DataStructure2 {
        val jsonObject = structure.data
        if (jsonObject.containsKey("gender")) {
            when (val gender = jsonObject["gender"]) {
                is JsonArray<*> -> {
                    for (item in gender) {
                        when (item) {
                            is JsonObject -> {
                                when (item["id"] as String) {
                                    "https://d-nb.info/standards/vocab/gnd/gender#notKnown" -> {
                                        jsonObject["wdt:P21"] = "http://www.wikidata.org/entity/Q24238356"
                                    }
                                    "https://d-nb.info/standards/vocab/gnd/gender#male" -> {
                                        jsonObject["wdt:P21"] = "http://www.wikidata.org/entity/Q6581097"
                                    }
                                    "https://d-nb.info/standards/vocab/gnd/gender#female" -> {
                                        jsonObject["wdt:P21"] = "http://www.wikidata.org/entity/Q6581072"
                                    }
                                }
                            }
                        }
                    }

                }
            }
            jsonObject.remove("gender")
        }
        return structure
    }

    fun extractBirthAndDeathYear(key: String, structure: DataStructure2): DataStructure2 {
        extractYear("dateOfBirth", "dbo:birthYear", key, structure.data)
        extractYear("dateOfDeath", "dbo:deathYear", key, structure.data)
        return structure
    }

    private fun extractYear(property: String, target: String, key: String, jsonObject: JsonObject) {
        if (jsonObject.containsKey(property)) {
            val date = jsonObject[property]
            if (date is JsonArray<*>) {
                val matcher = yearPattern.matcher(date[0] as String)
                if (matcher.find()) {
                    jsonObject[target] = matcher.group(1)
                } else {
                    log.warn("Could not parse year in ${date[0]} from resource $key.")
                }
                if (date.size > 1) {
                    log.warn("More than one date in resource $key. Only parsed the first occurrence!")
                }
            } else if (date is String){
                val matcher = yearPattern.matcher(date)
                if (matcher.find()) {
                    jsonObject[target] = matcher.group(1)
                } else {
                    log.warn("Could not parse year in $date from resource $key.")
                }
            }
        }
    }

    fun decodeDbpediaUrl(structure: DataStructure2): DataStructure2 {
        val jsonObject = structure.data
        if (!jsonObject.containsKey("sameAs")) return structure
        val jsonArray = jsonObject.array<JsonObject>("sameAs")?.toList() ?: return structure
        for (item in jsonArray) {
            if (item.containsKey("id")) {
                val id = item["id"] as String
                if (id.contains(Regex("dbpedia.org")) or id.contains(Regex("wikipedia.org"))) {
                    val decodedUrl = URLDecoder.decode(id, "UTF-8")
                    item["id"] = decodedUrl
                }
            }
        }
        return structure
    }

    fun addMetadata(structure: DataStructure2): List<SbMetadataModel> {
        for (type in structure.types) {
            val index = mapping.string(type)
            if (index == null)
                continue
            else {
                return listOf(
                    SbMetadataModel()
                        .setData(Klaxon().toJsonString(structure.data))
                        .setEsIndexName("$index-${structure.date}")
                        .setEsBulkAction(EsBulkActions.INDEX)
                )
            }
        }
        log.error("No known type in resource: ${structure.types}")
        return emptyList()
    }
}