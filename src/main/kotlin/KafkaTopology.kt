/*
 * gnd-import
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import java.util.*

class KafkaTopology(private val properties: Properties, log: Logger) {

    private val gndTransform = GndTransform(properties, log)

    fun build(): Topology {
        val builder = StreamsBuilder()
        builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .flatMapValues { value -> gndTransform.parseJson(value) }
            .flatMap { _, value -> gndTransform.extractKey(value) }
            .flatMapValues {key,  value -> gndTransform.extractTypes(key, value) }
            .mapValues { value -> gndTransform.expandTypes(value) }
            .mapValues { value -> gndTransform.replaceGender(value) }
            .mapValues { key, value -> gndTransform.extractBirthAndDeathYear(key, value) }
            .mapValues { value -> gndTransform.decodeDbpediaUrl(value) }
            .flatMapValues { value -> gndTransform.addMetadata(value) }
            .to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }
}
