import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.linked.KafkaProperties
import org.swissbib.linked.KafkaTopology
import org.swissbib.types.EsBulkActions
import java.io.File
import java.nio.charset.Charset

class ParserTests {

    private val log = LogManager.getLogger()
    private val props = KafkaProperties(log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties);

    private val resourcePath = "src/test/resources"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun testAddMetadata() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"), null,
            SbMetadataModel().setData(readFile("input1.json")).setMessageDate("2019-10-11")))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://d-nb.info/gnd/1155619099", output.key())
        assertEquals("gnd-persons-2019-10-11", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output1.json"), output.value().data)

    }

    @Test
    fun testAddMetadata2() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            null,
            SbMetadataModel().setData(readFile("input2.json")).setMessageDate("2019-10-11")))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://d-nb.info/gnd/141701803", output.key())
        assertEquals("gnd-persons-2019-10-11", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output2.json"), output.value().data)
    }

    @Test
    fun testAddMetadata3() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            null,
            SbMetadataModel().setData(readFile("input3.json")).setMessageDate("2019-10-11")))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://d-nb.info/gnd/118823574", output.key())
        assertEquals("gnd-persons-2019-10-11", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output3.json"), output.value().data)
    }

}